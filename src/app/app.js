angular.module( 'angulatar', [
  'templates-app',
  'templates-common',
  'angulatar.home',
  'ui.router'
])

.config(function ( $stateProvider, $urlRouterProvider ) {
  $urlRouterProvider.otherwise( '/' );
})

.run( function run () {
})

.controller( 'AppCtrl', ['$scope', '$location', function( $scope, $location ) {
  $scope.$on('$stateChangeSuccess', function(event, toState, toParams, fromState, fromParams){
    if ( angular.isDefined( toState.data.pageTitle ) ) {
      $scope.pageTitle = toState.data.pageTitle + ' | angulatar' ;
    }
  });
}]);

