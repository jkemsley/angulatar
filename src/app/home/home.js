
angular.module('angulatar.home', [
  'ui.router',
  'angulatar.avatarpart'
])


.config(function( $stateProvider ) {
  $stateProvider.state( 'home', {
    url: '/',
    views: {
      "main": {
        controller: 'HomeCtrl',
        templateUrl: 'home/home.tpl.html'
      }
    },
    data:{ pageTitle: 'Home' }
  });
})

/**
 * And of course we define a controller for our route.
 */
.controller( 'HomeCtrl', ['$scope', function( $scope ) {
        $scope.andriodcolor = '#a4c639';

        $scope.options = {
            face: [
                '<circle cx="-42" r="4" stroke="#FFF" fill="#FFF" stroke-width="7.2"/><circle cx="42" r="4" stroke="#FFF" fill="#FFF" stroke-width="7.2"/>',
                '<circle cx="-42" r="4" stroke="#FFF" fill="#FFF" stroke-width="7.2"/><path d="M 38 0 a 6.8 6.8, 0, 0, 0, 13.6 0" stroke-width="1.2" stroke="#FFF"/>'
            ],
            arms: [
                '<use x="-143" y="41" xlink:href="#limb"/><use x="95" y="41" xlink:href="#limb"/>',
                '<use x="-143" y="-41" xlink:href="#limb"/><use x="95" y="-41" xlink:href="#limb"/>'
            ],
            legs: [
                '<use x="-58" y="138" xlink:href="#limb"/><use x="10" y="138" xlink:href="#limb"/>',
                '<use x="-58" y="138" xlink:href="#limb"/><path d="M 10 138 h 48 v 70 a 24 24, 0, 0, 1, 0 48 l -48 0 z" stroke="#FFF" stroke-width="7.2"></path>'
            ],
            antenna: [
                '<use y="-86" x="14" transform="rotate(29)" xlink:href="#antenna"/><use y="-86" x="-27" transform="rotate(-29)" xlink:href="#antenna"/>',
                '<use y="-70" x="0" transform="rotate(20)" xlink:href="#antenna"/><use y="-70" x="-13" transform="rotate(-20)" xlink:href="#antenna"/>'
            ],
            tshirt: [
                'assets/ubuntu.svg',
                'assets/linux.svg',
                'assets/starbucks.svg'
            ]
        };

        $scope.selection = {
            face: 0,
            arms: 0,
            legs: 0,
            antenna: 0,
            tshirt: 0
        };
}]);

