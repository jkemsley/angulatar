angular.module('angulatar.avatarpart', [])

.directive('avatarPart', function() {
   return {
       restrict: 'A',
       scope: {
           options: '=',
           current: '='
       },
       link: function(scope, element, atts) {
           scope.$watch('current', function() {
               element.html(scope.options[scope.current]);
           }, true);
       }
   };
});